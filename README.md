##

electron + typescript でtypescriptを触ってみる

## 入れるもの

```
    "electron": "^2.0.6",
    "ts-loader": "^4.4.2",
    "typescript": "^3.0.1",
    "typings": "^2.1.1",
    "webpack": "^4.16.5",
    "webpack-cli": "^3.1.0"
```

## HTML

以下みたいに書いてあるhtmlがあるときに、TypeScript側で定義した`hoge()`が呼ばれない
```
(html)

<button class="btn" onClick="hoge()">
```

TypeScript側で、`addEventListener`を使って、定義するほうが良さそう
```
document.querySelector("btn").addEventListener("click", func)
```


HTMLに書いてある`<script>`タグに、`defer`を渡すと、HTMLパース完了後に読み込まれる  
これがなくて、なんどかはまった  
https://qiita.com/phanect/items/82c85ea4b8f9c373d684

## typescript

`tsconfig.json`ファイルを作成する  
`tsc --init`  

`tsconfig.json`は、エントリポイントになってるjsファイルと同じ階層に置かないとダメそう。


`tsconfig.json`で指定できる内容  
http://neos21.hatenablog.com/entry/2017/10/24/080000

## typings

`npm install -g typings`
グローバルにないとパスが通ってないので、うまくコマンドが・・・。  
`node_module`のなかにあるかな？

typingsの初期化と必要な定義を落としてくる  
```
typings init
typings install --global --save electron --source dt
```

`src/index.ts`を作って、定義ファイルを読み込ませる
```
(index.ts)

/// <reference path="../typings/globals/electron/index.d.ts" />
定義ファイルへのパスを１行目に書くらしい
```

`/// <reference path="typings/index.d.ts>` こうすると、全部の定義読み込んでくれるらしいけど、`tsc`コマンドでエラーが出る
```
k.soga@mihirogi ~/w/typescript-electron-sample (master)> tsc index.ts
index.ts:4:29 - error TS2552: Cannot find name 'Electron'. Did you mean 'electron'?

4 const BrowserWindow: typeof Electron.BrowserWindow = electron.BrowserWindow;
                              ~~~~~~~~

  index.ts:3:7
    3 const electron = require('electron');
            ~~~~~~~~
    'electron' is declared here.

index.ts:5:12 - error TS2503: Cannot find namespace 'Electron'.

5 const app: Electron.App = electron.app;
             ~~~~~~~~

index.ts:8:17 - error TS2503: Cannot find namespace 'Electron'.

8     mainWindow: Electron.BrowserWindow = null;
                  ~~~~~~~~

index.ts:10:29 - error TS2503: Cannot find namespace 'Electron'.

10     constructor(public app: Electron.App){
                               ~~~~~~~~
```

tsファイルをjsファイルにコンパイル  
`tsc src/index.ts`

実行
`npx electron ./src/`


## webpack

`npm run build`でビルドするようにしたかったので、以下を追加したけどエラーが出た
```
(package.json)

  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "build": "webpack"
  },
```

```
ERROR in ./node_modules/electron/index.js
Module not found: Error: Can't resolve 'fs' in '/Users/k.soga/work/typescript-electron-sample/node_modules/electron'
 @ ./node_modules/electron/index.js 1:9-22
```

少し調べたら、`webpack.config.js`に`target`オプションを追加する必要があるらしい。  
http://karuta-kayituka.hatenablog.com/entry/2018/04/02/200207


## 勉強用でこれの構成とか読む

このリポジトリ
https://github.com/tarunlalwani/electron-webpack-typescript-boilerplate


* 階層ごとに`tsconfig.json`を置く必要がある？
* `webpack.config.js`の`output`に`filename`を個別で別名になるように指定した

## わからん

`--ambient` オプションってなんだこれ  
https://kuroeveryday.blogspot.com/2016/02/typescript-typings.html



