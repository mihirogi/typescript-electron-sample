module.exports = [{
    // モード値を production に設定すると最適化された状態で、
    // development に設定するとソースマップ有効でJSファイルが出力される
    mode: 'development',

    // メインとなるJavaScriptファイル（エントリーポイント）
    entry: './src/main/index.ts',

    target:"electron-main", //electron用ということを指定しないといけないらしい

    output: {
        //  出力ファイルのディレクトリ名
        path: `${__dirname}/dist`,
        filename: "index.js"
    },
        module: {
        rules: [
            {
                // 拡張子 .ts の場合
                test: /\.ts$/,
                // TypeScript をコンパイルする
                use: 'ts-loader'
            }
        ]
    },
    // import 文で .ts ファイルを解決するため
    resolve: {
        extensions: [
            '.ts','js'
        ]
    }
},
    {
        // モード値を production に設定すると最適化された状態で、
        // development に設定するとソースマップ有効でJSファイルが出力される
        mode: 'development',

        // メインとなるJavaScriptファイル（エントリーポイント）
        entry: './src/renderer/sub.ts',

        target:"electron-renderer", //electron用ということを指定しないといけないらしい

        output: {
            //  出力ファイルのディレクトリ名
            path: `${__dirname}/dist`,
            filename: "sub.js"
        },
        module: {
            rules: [
                {
                    // 拡張子 .ts の場合
                    test: /\.ts$/,
                    // TypeScript をコンパイルする
                    use: 'ts-loader'
                }
            ]
        },
        // import 文で .ts ファイルを解決するため
        resolve: {
            extensions: [
                '.ts','js'
            ]
        }
    }];